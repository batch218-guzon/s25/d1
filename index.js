// console.log("Hello world")

// [SECTION] JSON Objects
/*
	-JSON stands for JavaScript Object Notation
	-A commmon use of JSON is to read data from a web server, and display the data in a web page
	-Features of JSON:
		-It is a lightweight data-interchange format
		-It is easy to read and write
		-It is easy for machines to parse and generate
*/

// JSON Objects
//  - JSON also use the "key/value pairs"
// 	- "Key/property" names requires to be enclosed with double quotes.
/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "value"
	}
*/

// Example of JSON Object
/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/
// Arrays of JSON Object
/*
"cities" : [
	{
		"city": "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city": "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city": "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
]
*/
console.log("----------------------------------------")
// [SECTION] Converting JavaScript Object to JSON Object
// JavaScript Array of Objects
// let batchesArr = [
// 	{
// 		batchName: "Batch 218",
// 		schedule: "Part Time"
// 	},
// 	{
// 		batchName: "Batch 219",
// 		schedule: "Full Time"
// 	}

// ]

// console.log(batchesArr);

// The "stringify" method is used to convert JavaScript Objects into a string
// console.log("Result of stringify method: ");
// console.log(JSON.stringify(batchesArr));

// let data = JSON.stringify({
// 	name: "John",
// 	age: 31,
// 	address: {
// 		city: "Manila",
// 		country: "Philippines"
// 	}
// })

// console.log(data);

// let firstName = prompt("Enter your first name: ");
// let lastName = prompt("Enter your last name: ");
// let email = prompt("Enter your email: ");
// let password = prompt("Enter your password: "); 

// let otherData = JSON.stringify({
// 	firstName : firstName,
// 	lastName : lastName,
// 	email : email,
// 	password : password
// });

// console.log(otherData);

// [SECTION] Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[
	{
		"batchName" : "Batch 218",
		"schedule" : "Part time"
	},
	{
		"batchName" : "Batch 219",
		"schedule" : "Full time"
	}
]`

console.log("batchesJSON content: ");
console.log(batchesJSON);

// JSON.parse method converts JSON Object into JavaScript Object
console.log("batchesJSON content: ");
console.log(JSON.parse(batchesJSON));

let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject))